## Video News & Content

| Vlogger | Twitter | Release Schedule | Latest Release |
| ------- | ------- | ---------------- | -------------- |
| [![Sockarina](http://a.pomf.se/llbbnc.png "Sockarina") **Sockarina**](https://www.youtube.com/channel/UCj6WU3IdNXHuwqv6WbcUH2Q) | [@Rinaxas](https://twitter.com/Rinaxas) | Daily | [3-4 Oct 2014](https://www.youtube.com/watch?v=Ra4yD_wHpvQ) |
| [![GamerGate News](http://a.pomf.se/wuajyk.png "GamerGate News") **GamerGate News**](https://www.youtube.com/channel/UC1FantLFzgT7FDrWB_7eUKg) | [@GamerGateNews](https://twitter.com/GamerGateNews) | Mon/Wed/Fri | [1 Oct 2014](https://www.youtube.com/watch?v=kAxUAvojQ7g) |