* Write boycott emails to [Vox Media's advertisers](http://theralphretort.com/operationvoxpopuli-advertiser-list/).
* Do not use the words GamerGate or #GamerGate anywhere in the emails.
* Mention that Vox Media properties (Vox, Polygon, The Verge) have spoken insultingly and hatefully against gamers, their core demographic. You can cite [some or all of the articles here](http://pastebin.com/CgVmS2pS).
* Also mention that they have gone so far as to [attack their own advertisers](https://archive.today/OSzto), and that advertising on these properties has become a complete liability.
* As soon as you send each email, paste its contents into a TwitLonger, fill in the details into the TwitLonger subject line (company, contact person, #GamerGate and #OperationVoxPopuli hashtags) and tweet. This part is optional, but increases the operation's impact.
* Favorite and retweet other people's #OperationVoxPopuli TwitLonger tweets. This part is optional, but increases the operation's impact.
* Do this on each Sunday and Wednesday night.