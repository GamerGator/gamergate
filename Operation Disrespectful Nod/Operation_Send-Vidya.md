Nvidia is said to be strongly considering pulling their ads from Kotaku.

They also advertise on at least Rock, Paper, Shotgun, and possibly other corrupt gaming publications.

Now is the time to email them and inform them of exactly why they should pull their ads from all corrupt gaming publications. Tell them that these publications collude anti-competitively and ideologically to perpetrate anti-gamer hate, that there is a consumer revolt against them, and that severing ties with them would be a selling point, especially during the holiday season. Feel free to extend this message further with other personal points, but these points are key.

The worst offenders, all of which you should encourage Nvidia to pull ads from and/or not advertise in going forward, are as follows:

* [Kotaku](http://kotaku.com/)
* [Rock, Paper, Shotgun](http://www.rockpapershotgun.com/)
* [Polygon](http://www.polygon.com/)
* [Destructoid](http://www.destructoid.com/)
* [Giant Bomb](http://www.giantbomb.com/)
* [Motherboard](http://motherboard.vice.com/)
* [IGN](http://www.ign.com/)
* [GameSpot](http://www.gamespot.com/)
* [Gamasutra](http://www.gamasutra.com/)
* [Gameranx](http://www.gameranx.com/)
* [Ars Technica](http://arstechnica.com/)

**Email addresses:**

* mlim at nvidia dot com - Micheal Lim, Industry Analyst Relations
* rsherbin at nvidia dot com - Bob Sherbin, Corp. Communications Lead
* bdelrizzo at nvidia dot com - Byran Del Rizzo, GeForce & Consumer Desktop Products

**Anti-gamer pieces:**

['Gamers' don't have to be your audience. 'Gamers' are over. Exclusive](http://archive.today/l1kTW)  
Leigh Alexander  
Gamasutra  
Aug 28, 10:00am  

[An awful week to care about video games](http://archive.today/rkvO8)  
Chris Plante  
Polygon  
Aug 28, 1:21pm  

[The death of the “gamers” and the women who “killed” them](https://archive.today/ZyLdw)  
Casey Johnson  
Ars Technica  
Aug 28, 5:00pm  

[A Guide to Ending "Gamers"](http://archive.today/2t93l)  
Devin Wilson  
Gamasutra  
Aug 28, 7:57 pm  

[We Might Be Witnessing The 'Death of An Identity'](https://archive.today/ht088)  
Luke Plunkett  
Kotaku  
Aug 28, 8:00pm   

[More anti-gamer pieces](http://pastebin.com/CgVmS2pS)

[Even more anti-gamer pieces](http://pastebin.com/TnR3SFHs)

Send links to more anti-gamer/anti-#GamerGate pieces in SJW gaming publications (or any other info) to GamerGate at boun dot cr.