This man is you friend: (Smallest images)

We'll eventually get around to adding the full deck.

Photoshop-Anons, remember to keep making these as new heroes enter the Stage of History.

Images:  
* [Adam Baldwin](http://a.pomf.se/yyvols.jpg)
* [Animal Mother](http://a.pomf.se/hawymx.png)
* [Internet Aristocrat](http://a.pomf.se/ycnevb.png)
* [LEOpirate](http://a.pomf.se/iurylq.png)
* [Daniel Vávra](http://a.pomf.se/aboilb.png)
* [Milo Yiannopoulos](http://a.pomf.se/aqhhca.png)

Previews:

![Adam Baldwin](http://a.pomf.se/yyvols.jpg)
![Animal Mother](http://a.pomf.se/hawymx.png)
![Internet Aristocrat](http://a.pomf.se/ycnevb.png)
![LEOpirate](http://a.pomf.se/iurylq.png)
![Daniel Vávra](http://a.pomf.se/aboilb.png)
![Milo Yiannopoulos](http://a.pomf.se/aqhhca.png)
