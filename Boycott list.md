Many #GamerGate supporters are boycotting the following websites. These supporters request these websites look into changing their ethics policies, require employees to have more transparency, retract untrue/false articles, look into allegations made against their employees and act accordingly.  
Kotaku and all Gawker Media properties  
Gamasutra  
Polygon  
Rock Paper Shotgun  
Ars Technica  
Wired  
The Guardian  
Destructoid  
Gameranx  
BadAss Digest  
TechCrunch  
NEOGAF  

Many #GamerGate supporters are boycotting the following gaming companies. These supporters ask these companies look into the allegations made against their employees or the companies they choose to advertise with and act accordingly.  
Gearbox Software  
Doublefine  
Vlambeer  
Polytron  
Asobo  
Electronic Arts  

Many #GamerGate supporters are boycotting the following companies. These supporters ask these companies to pull advertisements from the following websites.  
Samsung - Kotaku, Polygon  
Verizon - Kotaku, Polygon  
NewEgg - Kotaku  
Electronic Arts - Kotaku  
Amazon - Kotaku  
Best Buy - Kotaku  
iTunes - Kotaku  
Spring - Kotaku  
Intel - Gamasutra  
Maya/Autodesk - Gamasutra  
Nvidia - Rock Paper Shotgun  

Many #GamerGate supporters request that the following people are asked to resign. There are multiple reasons for these resignations. They include, but are not limited to: bullying, speaking out against their audience in public, sexist behavior, racist behavior, threatening behavior, helping to create an industry wide social media blacklist, condoning/allowing harassment and bullying, colluding with their peers, representing their company in an unfair and unprofessional way.  
Stephen Totilo - Kotaku  
Ben Kuchera - Polygon  
Leigh Alexander - Gamasutra  
Alex Lifschitz - Asobo  
Jason Schrier - Kotaku  
Nathan Grayson - Kotaku  
Chris Grant - Polygon  
Anthony Burch - Gearbox Software  
Devin Faraci - BadAss Digest  
Kyle Orland - Ars Technica  
Tadgh Kelly - TechCrunch  
Ian Cheong - Gameranx  
 
Many #GamerGate supporters would like to request large gaming developers and companies to stop doing business with the following:  
Kotaku  
Polygon  
Rock Paper Shotgun  
Ars Technica  
Wired  
TechCrunch  
SilverString Media  
Anita Sarkeesian/Feminist Frequency  
Gamasutra  
